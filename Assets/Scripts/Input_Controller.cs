﻿using UnityEngine;
using System.Collections;

public class Input_Controller : MonoBehaviour
{
    public Base_SpaceShip_Controller Controllable_Spaceship = null;
    public GameObject Camera = null;

	void Start()
    {
	}
	
	void Update()
    {
	}

    void FixedUpdate()
    {
        float forward = Input.GetAxis("Forward");

        if (Controllable_Spaceship != null)
        {
            if (forward > 0)
                Controllable_Spaceship.GoForward(forward);
        }
    }
}
