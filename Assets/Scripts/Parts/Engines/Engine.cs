﻿using UnityEngine;
using System.Collections;

public class Engine : MonoBehaviour
{
    public float Mass = 2000.0f;
    public float Power = 100000.0f;

	void Start()
    {
        enabled = false;
	}

    public void ApplyForce(Rigidbody rigidbody, Vector3 direction)
    {
        rigidbody.AddForce(direction * Power);
    }
}
