﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Base_SpaceShip_Controller : MonoBehaviour
{
    public List<GameObject> EnginesPrefab = new List<GameObject>();
    public List<GameObject> WeaponsPrefab = new List<GameObject>();

    private List<Engine> _Engines = new List<Engine>();

    void Start()
    {
        for (int i = 0; i < EnginesPrefab.Count; i++)
        {
            GameObject enginePrefab = EnginesPrefab[i];

            if (enginePrefab != null)
            {
                GameObject placeholder = transform.FindChild("Parts/Engine_" + i).gameObject;
                GameObject engineClone = GameObject.Instantiate(enginePrefab) as GameObject;
                
                engineClone.transform.position = placeholder.transform.position;
                engineClone.transform.rotation = placeholder.transform.rotation;
                engineClone.transform.localScale = placeholder.transform.localScale;
                engineClone.transform.parent = placeholder.transform.parent;

                Engine engineComponent = engineClone.GetComponent<Engine>();
                _Engines.Add(engineComponent);
                rigidbody.mass += engineComponent.Mass;

                Destroy(placeholder);
            }
        }

        enabled = false; // Used to avoid Unity to call Update()
	}

    /* void Update() {} */

    public void GoForward(float input)
    {
        foreach (Engine engine in _Engines)
        {
            engine.ApplyForce(rigidbody, transform.forward * input);
        }
    }
}
